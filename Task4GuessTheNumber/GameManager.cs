﻿using System;
namespace Task4GuessTheNumber
{
    public
        class GameManager
    {
        private readonly IConsoleCommunication _consoleCommunication;
        private readonly GuessNumberGame _guessTheNumberGame;
        public GameManager(IConsoleCommunication consoleCommunication, GuessNumberGame guessTheNumberGame)
        {
            this._consoleCommunication = consoleCommunication ?? throw new ArgumentNullException(nameof(consoleCommunication));
            this._guessTheNumberGame = guessTheNumberGame ?? throw new ArgumentNullException(nameof(guessTheNumberGame));
        }
        public void Start()
        {
            _consoleCommunication.PrintMessage(SystemMessages.InputCommandRequest);

            while (true)
            {
                var command = _consoleCommunication.ReadCommand();
                switch (command)
                {

                    case Command.Exit:
                        break;
                    case Command.Help:
                        _consoleCommunication.WriteHelpMessage();
                        break;
                    case Command.Start:
                        GameRound();
                        break;
                    case Command.IncorrectCommand:
                        _consoleCommunication.PrintMessage(SystemMessages.IncorrectCommandError);
                        break;
                    default:
                        _consoleCommunication.PrintMessage(SystemMessages.IncorrectCommandError);
                        break;
                }
                if (Command.Exit == command)
                {
                    break;
                }
            }
        }
        private void GameRound()
        {
            _guessTheNumberGame.SetGameNumber();
            _consoleCommunication.PrintMessage(SystemMessages.GameIsStartedAlert);
            _consoleCommunication.PrintMessage(SystemMessages.InputNumRequest);
            int userNumber;
            Result result;
            do
            {
                userNumber = SetUserNumber();
                result = _guessTheNumberGame.GetResult(userNumber);
                PromptToPlayer(result);
            } while (result != Result.Equal);
        }
        private bool CheckUserInput(string userInput)
        {
            if (string.IsNullOrEmpty(userInput))
            {
                throw new ArgumentNullException(nameof(userInput));
            }
            int valueFlag;
            if (int.TryParse(userInput, out valueFlag))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private int SetUserNumber()
        {
            string inputVlaue = _consoleCommunication.GetInput();
            while (CheckUserInput(inputVlaue))
            {
                _consoleCommunication.PrintMessage(SystemMessages.IncorrectValueInputError);
                inputVlaue = _consoleCommunication.GetInput();
            }
            return Convert.ToInt32(inputVlaue);
        }
        private void PromptToPlayer(Result result)
        {
            switch (result)
            {
                case Result.Equal:
                    _consoleCommunication.PrintMessage(SystemMessages.PlayerWonAlert);
                    break;
                case Result.Less:
                    _consoleCommunication.PrintMessage(SystemMessages.LessThenNumPrompt);
                    break;
                case Result.More:
                    _consoleCommunication.PrintMessage(SystemMessages.MoreThenNumPrompt);
                    break;
            }

        }

    }
}
