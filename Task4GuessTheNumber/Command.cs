﻿namespace Task4GuessTheNumber
{
    public enum Command
    {
        Start,
        Exit,
        Help,
        IncorrectCommand
    }
}
