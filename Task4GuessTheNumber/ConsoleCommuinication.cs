﻿using System;
namespace Task4GuessTheNumber
{
    public class ConsoleCommunication : IConsoleCommunication
    {

        public string GetInput()
        {
            string message = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(message))
            {
                PrintMessage(SystemMessages.IncorrectValueInputError);
                message = Console.ReadLine();
            }
            return message;
        }
        public void PrintMessage(object message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }
            Console.WriteLine(message);
        }
        public Command ReadCommand()
        {
            string userInput = Console.ReadLine();
            if (Enum.TryParse(userInput, true, out Command command))
            {
                return command;
            }
            else
            {
                return Command.IncorrectCommand;
            }

        }
        public void WriteHelpMessage()
        {
            PrintMessage(SystemMessages.StartCommandDescription);
            PrintMessage(SystemMessages.ExitCommandDescription);
        }
    }
}
