﻿using System;
namespace Task4GuessTheNumber
{
    public class GuessNumberGame 
    {
        private int _gameNumber;
        private int _maxNumber, _minNumber;
        Random random = new Random();
        public GuessNumberGame() 
        {
            _maxNumber = 101;
            _minNumber = 0;
        }
        public GuessNumberGame(int userGameNumber)//for tests only
        {
            _maxNumber = userGameNumber;
            _minNumber = userGameNumber;
        }
        public Result GetResult(int userNumber)
        {
            if (userNumber == _gameNumber)
            {
                return Result.Equal;
            }
            else if (userNumber > _gameNumber)
            {
                return Result.More;
            }
            else
            {
                return Result.Less;
            }
        }
        public void SetGameNumber()
        {
            this._gameNumber = random.Next(_minNumber, _maxNumber);
        }
        
    }
}
