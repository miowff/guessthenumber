﻿namespace Task4GuessTheNumber
{
    public interface IConsoleCommunication
    {
        public string GetInput();
        public void PrintMessage(object message);
        public Command ReadCommand();
        public void WriteHelpMessage();
    }
}
