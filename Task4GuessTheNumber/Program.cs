﻿namespace Task4GuessTheNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleCommunication consoleCommunication = new ConsoleCommunication();
            GuessNumberGame guessNumberGame = new GuessNumberGame();
            GameManager gameManager = new GameManager(consoleCommunication, guessNumberGame);
            gameManager.Start();
        }
    }
}
