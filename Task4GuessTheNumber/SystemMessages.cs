﻿namespace Task4GuessTheNumber
{
    public class SystemMessages
    {
        public const string InputCommandRequest = "Enter command. Type <Help> to see all commands!";
        public const string IncorrectCommandError = "Incorrect command!Type <Help> to see all commands! ";
        public const string StartCommandDescription = "Start -> Starts new game";
        public const string ExitCommandDescription = "Exit -> Close this program";
        public const string IncorrectValueInputError = "Your input is not decimal number";
        public const string MoreThenNumPrompt = "Your number more than game-number";
        public const string LessThenNumPrompt = "Your number less than game-number";
        public const string GameIsStartedAlert = "Game is started!Good luck!";
        public const string InputNumRequest = "Enter your number!";
        public const string PlayerWonAlert = "You won! Type <Start> to play again!";
    }
}
