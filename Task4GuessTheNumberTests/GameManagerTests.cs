﻿using NUnit.Framework;
using Moq;
using Task4GuessTheNumber;
namespace Task4GuessTheNumberTests
{
    class GameManagerTests
    {
        private  Mock<IConsoleCommunication> _consoleCommunicationMoq = new Mock<IConsoleCommunication>();
        [Test]
        public void StartTest()
        {
            //Arrange
            GuessNumberGame guessNumberGame = new GuessNumberGame(10);
            GameManager _gameManager = new GameManager(_consoleCommunicationMoq.Object,guessNumberGame);
            _consoleCommunicationMoq.Setup(x => x.PrintMessage(It.IsAny<string>()));
            _consoleCommunicationMoq.SetupSequence(x => x.GetInput())
                 .Returns("10")
                 .Returns("10");
            _consoleCommunicationMoq.SetupSequence(x => x.ReadCommand())
                .Returns(Command.Start)
                .Returns(Command.Exit);
            //Act
            _gameManager.Start();
            //Assert
            _consoleCommunicationMoq.Verify(x => x.PrintMessage(It.IsAny<string>()),Times.AtLeastOnce);
        }
        [Test]
        public void HelpTest()
        {
            //Arrange
            GuessNumberGame guessNumberGame = new GuessNumberGame(10);
            GameManager _gameManager = new GameManager(_consoleCommunicationMoq.Object, guessNumberGame);
            _consoleCommunicationMoq.Setup(x => x.PrintMessage(It.IsAny<string>()));
            _consoleCommunicationMoq.SetupSequence(x => x.ReadCommand())
                .Returns(Command.Help)
                .Returns(Command.Exit);
            //Act
            _gameManager.Start();
            //Assert
            _consoleCommunicationMoq.Verify(x => x.WriteHelpMessage(), Times.Once);
        }

        [Test]
        public void IncorrectCommandTest()
        {
            //Arrange
            GuessNumberGame guessNumberGame = new GuessNumberGame(10);
            GameManager _gameManager = new GameManager(_consoleCommunicationMoq.Object, guessNumberGame);
            _consoleCommunicationMoq.Setup(x => x.PrintMessage(It.IsAny<string>()));
            _consoleCommunicationMoq.SetupSequence(x => x.ReadCommand())
                .Returns(Command.IncorrectCommand)
                .Returns(Command.Exit);
            //Act
            _gameManager.Start();
            //Assert
            _consoleCommunicationMoq.Verify(x => x.PrintMessage("Incorrect command!Type <Help> to see all commands! "),Times.AtLeastOnce);
        }

        [Test]
        public void ExitCommandTest()
        {
            //Arrange
            GuessNumberGame guessNumberGame = new GuessNumberGame(10);
            GameManager _gameManager = new GameManager(_consoleCommunicationMoq.Object, guessNumberGame);
            _consoleCommunicationMoq.Setup(x => x.PrintMessage(It.IsAny<string>()));
            _consoleCommunicationMoq.SetupSequence(x => x.ReadCommand())
                .Returns(Command.Exit);
            //Act
            _gameManager.Start();
            //Assert
            _consoleCommunicationMoq.Verify(x => x.PrintMessage("Enter command. Type <Help> to see all commands!"), Times.AtLeastOnce);
        }
    }
}
