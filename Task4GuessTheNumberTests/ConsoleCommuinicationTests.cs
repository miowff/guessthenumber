using NUnit.Framework;
using System;
using System.IO;
using Task4GuessTheNumber;
namespace ConsoleCommuinicationTests
{
    public class ConsoleCommunicationTests
    {
        ConsoleCommunication _consoleCommunication = new ConsoleCommunication();
        [Test]
        public void GetInputTest()
        {
            //Arrange
            string expected = "3";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            var actual = _consoleCommunication.GetInput();
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void PrintMessageTest()
        {
            //Arrange
            var output = new StringWriter();
            Console.SetOut(output);
            //Act
            _consoleCommunication.PrintMessage("message");
            //Assert
            Assert.That(output.ToString(), Is.EqualTo(string.Format("message\r\n", Environment.NewLine)));
        }
        [Test]
        public void NullMessageTest()
        {
            // Arrange
            string expected = null;
            //Assert
            Assert.Throws<ArgumentNullException>(() => _consoleCommunication.PrintMessage(expected));
        }

        [Test]
        public void ReadStartCommandTest()
        {
            //Arrange
            string expected = "Start";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            Command command = _consoleCommunication.ReadCommand();
            //Assert
            Assert.AreEqual(command,Command.Start);
        }

        [Test]
        public void ReadHelpCommandTest()
        {
            //Arrange
            string expected = "Help";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            Command command = _consoleCommunication.ReadCommand();
            //Assert
            Assert.AreEqual(command, Command.Help);
        }
        [Test]
        public void ReadExitCommandTest()
        {
            //Arrange
            string expected = "Exit";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            Command command = _consoleCommunication.ReadCommand();
            //Assert
            Assert.AreEqual(command, Command.Exit);
        }
        [Test]
        public void ReadIncorrectCommandTest()
        {
            //Arrange
            string expected = "SomeIncorrectCommand";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            Command command = _consoleCommunication.ReadCommand();
            //Assert
            Assert.AreEqual(command, Command.IncorrectCommand);
        }
    }
}