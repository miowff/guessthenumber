﻿using NUnit.Framework;
using Task4GuessTheNumber;
namespace Task4GuessTheNumberTests
{
    public class GuessTheNumberTests
    {
        private readonly GuessNumberGame _guessNumber = new GuessNumberGame(10);
        [Test]
        public void GuessNumberIsNotNull()
        {
            //Assert
            Assert.IsNotNull(_guessNumber);
        }
        [Test]
        public void GetResultTest()
        {
            //Assert
            Assert.AreSame(Result.Equal.GetType(), _guessNumber.GetResult(10).GetType());
        }

        [Test]
        public void GetEqualResultTest()
        {
            //Arrange
            _guessNumber.SetGameNumber();
            //Act
            Result result = _guessNumber.GetResult(10);
            //Assert
            Assert.AreEqual(Result.Equal,result);
        }

        [Test]
        public void GetMoreResultTest()
        {
            //Arrange
            _guessNumber.SetGameNumber();
            //Act
            Result result = _guessNumber.GetResult(15);
            //Assert
            Assert.AreEqual(Result.More,result);
        }

        [Test]
        public void GetLessResultTest()
        {
            //Arrange
            _guessNumber.SetGameNumber();
            //Act
            Result result = _guessNumber.GetResult(1);
            //Assert
            Assert.AreEqual(Result.Less,result);
        }
    }
}
